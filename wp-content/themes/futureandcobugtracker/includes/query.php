<?php
/*
Our custom query including filtering for different users
*/
function fco_get_query_args() {
  global $wp_query;

  // Need to reset original query if single for widgets to work correctly
  if(is_single()){
    $wp_query = new WP_Query();
  }

  // Additional ticket filters (from parent theme)
  $args = $wp_query->query;
  $args['post_type'] = QC_TICKET_PTYPE;
  $args['orderby'] = 'modified';

  // If client is viewing
  if ( ! current_user_can( 'create_users' ) ) {
    // Only show tickets that haven't been set to hide from clients
    $args['meta_key'] = 'hide_client';
    $args['meta_value'] = 'true';
    $args['meta_compare'] = '!=';

    // Only show tickets from same project as client
    $fco_user_ID = get_current_user_id();
    $fco_user_projects = get_the_terms($fco_user_ID,'project');
    if($fco_user_projects) {
      $fco_slug = $fco_user_projects[0]->slug;

      $args['tax_query'] = array(
        array(
          'taxonomy' => 'project',
          'field'    => 'slug',
          'terms'    => $fco_slug,
        ),
      );
    }
  // If developer is viewing
  } else if ( ! current_user_can( 'manage_options' ) ) {
    global $wpdb;
    $fco_user_ID = get_current_user_id();
    // Get tickets assigned to developer
    $results = $wpdb->get_results($wpdb->prepare('SELECT p2p_to FROM wp_p2p WHERE p2p_type = "qc_ticket_assigned" AND p2p_from = %s', $fco_user_ID));
    foreach($results as $result) {
      $includes[] = $result->p2p_to;
    }
    // Only include the assigned tickets
    $args['post__in'] = $includes;
  }
  // Additional ticket filters (from parent theme)
  $remove_args = array( 'pagename', 'page_id' );
  $args = array_diff_key( $args, array_flip( $remove_args ) );
  // Get query data
  $fco_results = new WP_Query( $args );
  // Need to reset original query for widgets to work correctly
  $wp_query = new WP_Query();
  // Return query data
  return $fco_results;
}

?>