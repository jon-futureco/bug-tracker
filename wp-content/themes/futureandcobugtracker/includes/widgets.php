<?php

// Creating the team widget
class fco_team_widget extends WP_Widget {

  function __construct() {
    parent::__construct(
      // Base ID of your widget
      'fco_team_widget',

      // Widget name will appear in UI
      __('Future&Co Team Widget', 'fco_team_widget_domain'),

      // Widget description
      array( 'description' => __( 'Team widget to only display developers', 'fco_team_widget_domain' ), )
    );
  }

  // Creating widget front-end
  // This is where the action happens
  public function widget( $args, $instance ) {
    $title = apply_filters( 'widget_title', $instance['title'] );
    // before and after widget arguments are defined by themes
    echo $args['before_widget'];
    if ( ! empty( $title ) )
      echo $args['before_title'] . $title . $args['after_title'];

    $roles = array('administrator','developer');

    if(current_user_can('manage_options')){
      array_push($roles, 'client');
    }

    echo '<ul>';

    foreach ($roles as $role) {
      $users = get_users("role=$role");
      foreach ( $users as $user ) {
        echo '<li>';

        printf(
          '%3$s%4$s',
          get_author_posts_url( $user->ID, $user->user_nicename ),
          sprintf( __( 'View tickets by %s', APP_TD ), $user->user_nicename ),
          get_avatar( $user->ID, '28' ),
          $user->display_name
        );

        echo '</li>';
      }
    }

    echo '</ul>';


    echo $args['after_widget'];
  }

  // Widget Backend
  public function form( $instance ) {
    if ( isset( $instance[ 'title' ] ) ) {
      $title = $instance[ 'title' ];
    } else {
      $title = __( 'Project Team', 'fco_team_widget_domain' );
    }
    // Widget admin form
    ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
    </p>
    <?php
  }

  // Updating widget replacing old instances with new
  public function update( $new_instance, $old_instance ) {
  $instance = array();
  $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
  return $instance;
  }
} // Class wpb_widget ends here

// Creating the recent tickets widget
class fco_recent_widget extends WP_Widget {

  function __construct() {
    parent::__construct(
      // Base ID of your widget
      'fco_recent_widget',

      // Widget name will appear in UI
      __('Future&Co Recent Tickets Widget', 'fco_recent_widget_domain'),

      // Widget description
      array( 'description' => __( 'The most recent tickets', 'fco_recent_widget_domain' ), )
    );
  }

  // Creating widget front-end
  // This is where the action happens
  public function widget( $args, $instance ) {
    $title = apply_filters( 'widget_title', $instance['title'] );
    // before and after widget arguments are defined by themes
    echo $args['before_widget'];
    if ( ! empty( $title ) )
      echo $args['before_title'] . $title . $args['after_title'];

    $show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

    echo '<ul>';
    $fco_query = fco_get_query_args();

    if ( $fco_query->have_posts() ) : $i = 0;
      while ( $fco_query->have_posts() ) : $fco_query->the_post(); $i++;
        echo '<li>';

        echo html( 'a', array( 'href' => get_permalink(), 'title' => esc_attr( get_the_title() ) ), get_the_title() );

        if ( $show_date ) {
          echo html( 'span class="post-date"', get_the_date() );
        }

        echo '</li>';
      endwhile;
    endif;

    echo '</ul>';


    echo $args['after_widget'];
  }


  // Widget Backend
  public function form( $instance ) {
    $title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
    $number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
    $show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
?>
    <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'fco_recent_widget_domain' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

    <p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of tickets to show:', 'fco_recent_widget_domain' ); ?></label>
    <input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>

    <p><input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
    <label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Display ticket date?', 'fco_recent_widget_domain' ); ?></label></p>
<?php
  }

  // Updating widget replacing old instances with new
  public function update( $new_instance, $old_instance ) {
    $instance = $old_instance;
    $instance['title'] = strip_tags( $new_instance['title'] );
    $instance['number'] = (int) $new_instance['number'];
    $instance['show_date'] = (bool) $new_instance['show_date'];
    return $instance;
  }

} // Class wpb_widget ends here


// Creating the recently updated tickets widget
class fco_updated_widget extends WP_Widget {

  function __construct() {
    parent::__construct(
      // Base ID of your widget
      'fco_updated_widget',
      // Widget name will appear in UI
      __('Future&Co Recent Ticket Updates Widget', 'fco_updated_widget_domain'),
      // Widget description
      array( 'description' => __( 'The most recently updated tickets', 'fco_updated_widget_domain' ), )
    );
  }

  // Creating widget front-end
  public function widget( $args, $instance ) {
    $title = apply_filters( 'widget_title', $instance['title'] );
    $number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
    if ( ! $number ) {
      $number = 5;
    }
    // before and after widget arguments are defined by themes

    $comments = get_comments( array( 'status' => 'approve', 'post_status' => 'publish', 'post_type' => QC_TICKET_PTYPE ) );
    $output .= $before_widget;

    echo $args['before_widget'];
    if ( ! empty( $title ) )
      echo $args['before_title'] . $title . $args['after_title'];

    $output .= '<ul id="recent-tickets-updates">';
    if ( $comments ) {
      $fco_loop = 0;
      $fco_user_ID = get_current_user_id();
      $fco_user_projects = get_the_terms($fco_user_ID,'project');
      $fco_projects_array = array();

      foreach( $fco_user_projects as $fco_project ){
        array_push($fco_projects_array, $fco_project->name);
      }

      foreach ( (array) $comments as $comment) {
        $fco_match = false;
        $fco_id = $comment->comment_post_ID;
        $assignedto = _qc_assigned_to_users( $fco_id );
        foreach($assignedto as $assigned){
          if($assigned->ID === get_current_user_id()){
            $fco_match = true;
            break;
          }
        }
        if(!$fco_match){
          $fco_hide = get_post_meta($fco_id, 'hide_client', true);
          $terms = get_the_terms($fco_id,'project');
          if($terms) {
            foreach ( $terms as $term ) {
              $fco_name = $term->name;
              if( in_array($term->name, $fco_projects_array) && $fco_hide !== "true" ){
                $fco_match = true;
                break;
              }
            }
          }
        }

        if($fco_match) {
          $output .= html( 'li', sprintf(
            _x( '%1$s on %2$s', 'widget recent updates', APP_TD ),
            $comment->comment_author,
            html_link( get_comment_link( $comment->comment_ID ), get_the_title( $comment->comment_post_ID ) )//'<a href="' . esc_url( get_comment_link($comment->comment_ID) ) . '">' . get_the_title($comment->comment_post_ID) . '</a>'
          ) );
          $fco_loop++;
          if($fco_loop === $number) break;
        }

      }
    }
    $output .= '</ul>';
    $output .= $after_widget;
    echo $output;

    echo $args['after_widget'];
  }


  // Widget Backend
  public function form( $instance ) {
    $title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
    $number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
    $show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
?>
    <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'fco_updated_widget_domain' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

    <p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of tickets to show:', 'fco_updated_widget_domain' ); ?></label>
    <input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>

<?php
  }

  // Updating widget replacing old instances with new
  public function update( $new_instance, $old_instance ) {
  $instance = $old_instance;
    $instance['title'] = strip_tags( $new_instance['title'] );
    $instance['number'] = (int) $new_instance['number'];
  return $instance;
  }

} // Class wpb_widget ends here

// Register and load the widget
function fco_load_widget() {
  register_widget( 'fco_team_widget' );
  register_widget( 'fco_recent_widget' );
  register_widget( 'fco_updated_widget' );
}

add_action( 'widgets_init', 'fco_load_widget' );