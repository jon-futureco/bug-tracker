<?php
/*
Functions to allow duplicate comments that WordPress wouldn't allow.
Attaches random data to the comment, saves it and then removes the random data so only the intended comment is seen.
*/
function enable_duplicate_comments_preprocess_comment($comment_data){
  $random = md5(time());
  $comment_data['comment_content'] .= "disabledupes{" . $random . "}disabledupes";

  return $comment_data;
}
add_filter('preprocess_comment', 'enable_duplicate_comments_preprocess_comment');

function enable_duplicate_comments_comment_post($comment_id) {
  global $wpdb;
  //remove the random content
  $comment_content = $wpdb->get_var("SELECT comment_content FROM $wpdb->comments WHERE comment_ID = '$comment_id' LIMIT 1");
  $comment_content = preg_replace("/disabledupes{.*}disabledupes/", "", $comment_content);
  $wpdb->query("UPDATE $wpdb->comments SET comment_content = '" . $wpdb->escape($comment_content) . "' WHERE comment_ID = '$comment_id' LIMIT 1");
}
add_action('comment_post', 'enable_duplicate_comments_comment_post');

/*
Automatically generate comment (required by theme) when project or hide from client options are changed.
*/
function my_pre_comment_on_post($post_id){
  if ( !current_user_can( 'edit_user' ) )
    return false;

  $change = false;
  $comment_content = ( isset( $_POST['comment'] ) ) ? trim( $_POST['comment'] ) : null;


  $comment = "<ol class='update-list'>";
  if ( empty( $comment_content ) ){
    $project_comment = trim( $_POST['project'] );
    $terms = get_the_terms($post_id,'project');
    if($terms) {
      $project_old = $terms[0]->slug;
    }
    $ID = get_current_user_id();
    $hide = trim( $_POST['hide_client'] );
    $hide_old = get_post_meta($ID, 'hide_client', true);
    if ($project_comment !== $project_old ) {
      $change = true;
    }
    if ($hide !== $hide_old){
      $change = true;
    }
    if($change)
      $comment .= "<li><strong class='taxonomy'>Settings</strong> changed.</li>";
  }
  $comment .= "</ol>";

  if($change){
    $_POST['comment'] = $comment;
  }
}

add_action('pre_comment_on_post', 'my_pre_comment_on_post');

?>