<?php

if ( function_exists('register_sidebar') ) {
  register_sidebar(array(
    'name' => 'Client Sidebar',
    'before_widget' => '<li id="%1$s" class="widget-container widget_%1$s">',
    'after_widget' => '</li>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
  ));
}

?>