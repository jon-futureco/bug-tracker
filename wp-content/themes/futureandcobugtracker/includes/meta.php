<?php

/**
 * Registers the 'project' taxonomy for users and tickets.
 */
function fco_register_user_taxonomy() {

   register_taxonomy(
    'project',
    array('user','ticket'),
    array(
      'public' => true,
      'labels' => array(
        'name' => __( 'Project' ),
        'singular_name' => __( 'Project' ),
        'menu_name' => __( 'Project' ),
        'search_items' => __( 'Search Project' ),
        'popular_items' => __( 'Popular Project' ),
        'all_items' => __( 'All Project' ),
        'edit_item' => __( 'Edit Project' ),
        'update_item' => __( 'Update Project' ),
        'add_new_item' => __( 'Add New Project' ),
        'new_item_name' => __( 'New Project Name' ),
        'separate_items_with_commas' => __( 'Separate projects with commas' ),
        'add_or_remove_items' => __( 'Add or remove projects' ),
        'choose_from_most_used' => __( 'Choose from the most popular projects' ),
      ),
      'capabilities' => array(
        'manage_terms' => 'edit_users',
        'edit_terms'   => 'edit_users',
        'delete_terms' => 'edit_users',
        'assign_terms' => 'edit_users',
      ),
      'meta_box_cb' => 'fco_edit_ticket_project_section'
    )
  );
}

fco_register_user_taxonomy();

/**
  *Used for displaying count of tickets in sub-menus
  */
function fco_update_project_count($taxID, $taxonomy) {
  // If user is client do search with tickets that are not hidden and belong to clients project
  if (!current_user_can( 'create_users' )){
    $args['meta_key'] = 'hide_client';
    $args['meta_value'] = 'true';
    $args['meta_compare'] = '!=';

    $fco_user_ID = get_current_user_id();
    $fco_user_projects = get_the_terms($fco_user_ID,'project');

    if($fco_user_projects) {
      $fco_slug = $fco_user_projects[0]->slug;

      $args['tax_query'] = array(
        array(
          'taxonomy' => 'project',
          'field'    => 'slug',
          'terms'    => $fco_slug,
        ),
        array(
          'taxonomy' => $taxonomy,
          'field'    => 'id',
          'terms'    => $taxID,
        )
      );
    }
    $clientQuery = new WP_Query( $args );
    return($clientQuery->post_count);
  } else { // else not client
    global $wpdb;
    // Count all instances of taxonomy
    $fco_terms = $wpdb->get_col( $wpdb->prepare( "SELECT object_id FROM $wpdb->term_relationships WHERE term_taxonomy_id = $taxID") );
    $count = count($fco_terms);
    // Remove any instances of links to user
    foreach($fco_terms as $fco_term){
      if(get_user_by('id', $fco_term) )
        $count--;
    }
    // If user is admin return count as no filters needed
    if(current_user_can( 'manage_options' )){
      return $count;
    // If user is developer match count to tickets the developer has been assigned to
    } else if (current_user_can( 'create_users' )){
      $fco_user_ID = get_current_user_id();
      $results = $wpdb->get_results($wpdb->prepare('SELECT p2p_to FROM wp_p2p WHERE p2p_type = "qc_ticket_assigned" AND p2p_from = %s', $fco_user_ID));
      foreach($results as $result) {
        $includes[] = $result->p2p_to;
      }
      foreach($includes as $assigned){
        foreach($fco_terms as $fco_term){
          if($assigned === $fco_term){
            $matched[] = $assigned;
            break;
          }
        }
      }
      return count($matched);
    }
  }
}

/* Adds the taxonomy page in the admin. */
add_action( 'admin_menu', 'fco_add_project_admin_page' );

/* Creates the admin page for the 'project' taxonomy under the 'Users' menu. */
function fco_add_project_admin_page() {

  $tax = get_taxonomy( 'project' );

  add_users_page(
    esc_attr( $tax->labels->menu_name ),
    esc_attr( $tax->labels->menu_name ),
    $tax->cap->manage_terms,
    'edit-tags.php?taxonomy=' . $tax->name
  );
}

/* Create custom columns for the manage project page. */
add_filter( 'manage_edit-project_columns', 'fco_manage_project_user_column' );

function fco_manage_project_user_column( $columns ) {

  unset( $columns['posts'] );

  $columns['used'] = __( 'Tickets' );

  return $columns;
}

/* Customize the output of the custom column on the manage projects page. */
add_action( 'manage_project_custom_column', 'fco_manage_project_column', 10, 3 );

/* Displays content for custom columns on the manage projects page in the admin. */
function fco_manage_project_column( $display, $column, $term_id ) {

  if ( 'used' === $column ) {
    $term = get_term( $term_id, 'project' );
    echo $term->count;
  }
}

/* Add section to the edit user page in the admin to select project. */
add_action( 'show_user_profile', 'fco_edit_user_project_section' );
add_action( 'edit_user_profile', 'fco_edit_user_project_section' );

/**
 * Adds an additional settings section on the edit user/profile page in the admin.  This section allows users to
 * select a project from a dropdown of terms from the project taxonomy.
 */
function fco_edit_user_project_section( $user ) {

  $tax = get_taxonomy( 'project' );

  /* Make sure the user can assign terms of the project taxonomy before proceeding. */
  if ( !current_user_can( 'manage_options' ) || !is_admin() )
    return;

  /* Get the terms of the 'project' taxonomy. */
  $terms = get_terms( 'project', array( 'hide_empty' => false ) ); ?>

  <h3><?php _e( 'Project' ); ?></h3>

  <table class="form-table">

    <tr>
      <th><label for="project"><?php _e( 'Select Project' ); ?></label></th>

      <td><select name="project" id="project"><?php

      /* If there are any project terms, loop through them and display checkboxes. */
      if ( !empty( $terms ) ) {

        foreach ( $terms as $term ) { ?>
          <option value="<?php echo esc_attr( $term->slug ); ?>" <?php selected( true, is_object_in_term( $user->ID, 'project', $term ) ); ?> /><?php echo esc_attr( $term->name ); ?></option>
        <?php }
      }

      /* If there are no project terms, display a message. */
      else {
        _e( 'There are no projects available.' );
      }

      ?></select></td>
    </tr>

  </table>
<?php }

function fco_edit_ticket_project_section( $user ) {

  /* Make sure the user can assign terms of the project taxonomy before proceeding. */
  if ( !current_user_can( 'manage_options' ) ) {
    if ( !is_admin() ) {
      return;
    }
  }

  $tax = get_taxonomy( 'project' );

  /* Get the terms of the 'project' taxonomy. */
  $terms = get_terms( 'project', array( 'hide_empty' => false ) ); ?>

  <select name="project" id="project"><?php

    /* If there are any project terms, loop through them and display checkboxes. */
    if ( !empty( $terms ) ) {

      foreach ( $terms as $term ) { ?>
        <option value="<?php echo esc_attr( $term->slug ); ?>" <?php selected( true, is_object_in_term( $user->ID, 'project', $term ) ); ?> /><?php echo esc_attr( $term->name ); ?></option>
      <?php }
    }

    /* If there are no project terms, display a message. */
    else {
      _e( 'There are no projects available.' );
    }

    ?></select>
<?php }

/* Update the project terms when the edit user page is updated. */
add_action( 'personal_options_update', 'fco_save_user_project_terms' );
add_action( 'edit_user_profile_update', 'fco_save_user_project_terms' );
add_action( 'publish_post', 'fco_save_ticket_project_terms', 10 );
add_action( 'edit_post', 'fco_save_ticket_project_terms', 10 );
add_action( 'save_post', 'fco_save_ticket_project_terms', 10 );

/* Saves the term selected. */
function fco_save_user_project_terms( $user_id ) {

  $tax = get_taxonomy( 'project' );

  /* Make sure the current user can edit the user and assign terms before proceeding. */
  if ( !current_user_can( 'edit_user', $user_id ) && current_user_can( $tax->cap->assign_terms ) )
    return false;

  $term = esc_attr( $_POST['project'] );

  /* Sets the terms (we're just using a single term) for the user. */
  wp_set_object_terms( $user_id, array( $term ), 'project', false);

  clean_object_term_cache( $user_id, 'project' );

  co_update_project_count();
}

function fco_save_ticket_project_terms( $ID, $post ) {
  $term = esc_attr( $_POST['project'] );
  /* Sets the terms. */
  if(trim($term) !== "")
    wp_set_object_terms( $ID, array( $term ), 'project', false);

  clean_object_term_cache( $ID, 'project' );

  /* Make sure the current user can edit the user and assign terms before proceeding. */
  if ( !current_user_can( 'create_users' ) )
    return false;

  $fco_hide = $_POST['hide_client'];
  update_post_meta($ID, 'hide_client', $fco_hide);

  clean_object_term_cache( $ID, 'hide_client' );

}

function fco_add_projects_form_frontend(){
  /* Get the terms of the 'project' taxonomy. */
  if ( current_user_can( 'create_users' ) ) {
  $terms = get_terms( 'project', array( 'hide_empty' => false ) );
  $fco_id = get_the_ID();
  $checked = '';
  if(get_post_meta($fco_id, 'hide_client', true) == "true") {
    $checked = ' checked="checked"';
  }

  $termsMatch = get_the_terms($fco_id,'project');
  if($termsMatch) {
    foreach ( $termsMatch as $match ) {
      $fco_project_match = $match;
    }
  }
  ?>
  <p>
    <input type="checkbox" name="hide_client"<?php echo $checked; ?> value="true" /><label class="check-label" for="hide_client">Hide from client</label>
  </p>
  <p class="inline-input">
    <label>Project:</label><select name="project" id="project"><?php

    /* If there are any project terms, loop through them and display checkboxes. */
    if ( !empty( $terms ) ) {
      $selected = '';
      foreach ( $terms as $term ) {

        if($fco_project_match === $term->slug){
          $select = ' selected="selected"';
        }
      ?>
        <option value="<?php echo esc_attr( $term->slug ); ?>"<?php selected( true, is_object_in_term( $fco_id, 'project', $term->slug) ); ?>><?php echo esc_attr( $term->name ); ?></option>
      <?php }
    }

    /* If there are no project terms, display a message. */
    else {
      _e( 'There are no projects available.' );
    }

    ?></select>
</p>
  <?php
} else {
  $fco_user_ID = get_current_user_id();
  if (is_object_in_term( $fco_user_ID, 'project' ) ) {
    $terms = get_terms( 'project', array( 'hide_empty' => false ) );
    foreach ( $terms as $term ) {
      if( is_object_in_term( $fco_user_ID, 'project', $term ) ){
        ?>
        <input type="hidden" name="project" id="project" value="<?php echo esc_attr( $term->slug ); ?>" />
        <?php
        break;
      }
    }
  }
}
}

add_action('qc_ticket_form_advanced_fields','fco_add_projects_form_frontend');

/* Save client hide setting */
function fco_save_client_hide($post_ID){
  if (!current_user_can('create_users')) {
    update_post_meta($post_ID,'hide_client',$_POST['hide_client']);
  }
}

add_action( 'wp_insert_post', 'fco_save_client_hide', 10, 2 );
add_action( 'wp_update_post', 'fco_save_client_hide', 10, 2 );

?>