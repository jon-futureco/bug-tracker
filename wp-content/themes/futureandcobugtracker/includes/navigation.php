<?php

function fco_add_navigation($taxonomy) {
  // Check to see if any sub-menu needed
  $statuses = get_categories( array(
    'taxonomy' => $taxonomy,
    'hide_empty' => 0,
    'orderby' => $orderby,
    'order' => $order
  ) );

  if ( empty( $statuses ) ) {
    return;
  }

  // Change sort order for ticket priority sub-menu (High, Medium, Low)
  if($taxonomy != 'ticket_priority'){
    $orderby = 'name';
    $order = 'ASC';
  } else {
    $orderby = 'id';
    $order = "DESC";
  }
  // Output sub-menu
  $tax_object = get_taxonomy( $taxonomy );
  echo '<li' . ( is_tax( $taxonomy ) ? ' class="current-tab"' : '' ) . '>';
  echo '<a href="#">' . $tax_object->labels->singular_name . '</a>';

  echo '<ul class="second-level children">';

  foreach ( $statuses as $status ) {
    $content = $status->name;
    // If no count add class to change background colour of count box
    $taxCount = fco_update_project_count($status->term_taxonomy_id, $taxonomy);
    if($taxCount != 0){
      $content = html( 'span', $taxCount ) . $content;
    } else {
      $content = html( 'span', array( 'class' => 'tax-empty' ), $taxCount ) . $content;
    }

    echo html( 'li', html( 'a', array(
      'href' => get_term_link( $status, $taxonomy ),
      'title' => sprintf( __( 'View all tickets marked %s', APP_TD ), $status->name )
    ), $content ) );
  }

  echo '</ul>';

  echo '</li>';
}

?>