<?php
// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

// Hide WordPress admin bar from all users except admin
add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
	if (!current_user_can('manage_options')) {
	  show_admin_bar(false);
	}
}
// Include files - query must be first as function is used by other files
require_once( dirname( __FILE__ ) . '/includes/query.php' );
require_once( dirname( __FILE__ ) . '/includes/comments.php' );
require_once( dirname( __FILE__ ) . '/includes/meta.php' );
require_once( dirname( __FILE__ ) . '/includes/navigation.php' );
require_once( dirname( __FILE__ ) . '/includes/sidebars.php' );
require_once( dirname( __FILE__ ) . '/includes/widgets.php' );