<div id="footer" class="container">
	<div class="row">
		<div class="ninecol">
			<div id="footer-wrap">
				<?php wp_nav_menu( array( 'theme_location' => 'footer', 'container' => false, 'menu_id' => 'footer-nav-menu', 'depth' => 1, 'fallback_cb' => false ) ); ?>
				<ul>
					<li><a target="_blank" href="http://www.appthemes.com/themes/qualitycontrol/" title="WordPress Issue Tracking Software"><?php _e( 'Issue Tracking Software', APP_TD ); ?></a> | <?php _e( 'Powered by', APP_TD ); ?> <a target="_blank" href="http://wordpress.org/" title="WordPress">WordPress</a></li>
					<li class="alignright"><?php printf( __( '%1$d queries. %2$s seconds.', APP_TD ), get_num_queries(), timer_stop() ); ?></li>
				</ul>
			</div>
		</div><!--.col-->
		<div class="threecol last">
		</div><!--.col-->
	</div><!--.row-->
</div><!--#footer .container-->
