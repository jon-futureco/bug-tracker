<?php
/*
Plugin Name: Future&co Add user roles
Description: Add custom user roles
Version: 0.1
*/

function add_roles_on_plugin_activation() {
  add_role( 'client', 'Client', array(
    'read' => true,
    'edit_posts' => true,
    'edit_others_posts' => true,
    'edit_published_posts' => true,
    'publish_posts' => true
  ) );
  add_role( 'developer', 'Developer', array(
    'read' => true,
    'edit_posts' => true,
    'edit_others_posts' => true,
    'edit_published_posts' => true,
    'publish_posts' => true,
    'create_users' => true,
    'edit_users' => true
  ) );
}
register_activation_hook( __FILE__, 'add_roles_on_plugin_activation' );
?>